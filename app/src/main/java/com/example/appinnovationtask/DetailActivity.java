package com.example.appinnovationtask;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailActivity extends AppCompatActivity {
    TextView questions;
    RadioButton optionA,optionB,optionC;
    DatabaseHelper my_db;
    ArrayList<HashMap<String, String>> userData;
    private ArrayList<ListModel> list_model = new ArrayList<>();
    String question,questionoptionA,questionoptionB,questionoptionC;
    Button sumbit;
    RadioGroup radioGroup;
    String ans;
    String id,correctanswer;
    String marks;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        my_db = new DatabaseHelper(this);
        userData = new ArrayList<>();
        question = getIntent().getStringExtra("questions");
        id = getIntent().getStringExtra("id");
        questionoptionA = getIntent().getStringExtra("optionA");
        questionoptionB = getIntent().getStringExtra("optionB");
        questionoptionC = getIntent().getStringExtra("optionC");
        correctanswer = getIntent().getStringExtra("correctanswer");
         radioGroup = (RadioGroup)findViewById(R.id.group);
        sumbit =  findViewById(R.id.sumbit);
        questions = findViewById(R.id.questions);
        optionA = findViewById(R.id.optionA);
        optionB = findViewById(R.id.optionB);
        optionC = findViewById(R.id.optionC);

        for (int i = 0; i < radioGroup .getChildCount(); i++) {
            ((RadioButton) radioGroup.getChildAt(0)).setText(questionoptionA);
            ((RadioButton) radioGroup.getChildAt(1)).setText(questionoptionB);
            ((RadioButton) radioGroup.getChildAt(2)).setText(questionoptionC);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.optionA:
                        // do operations specific to this selection
                        ans = optionA.getText().toString();
                        break;
                    case R.id.optionB:
                        ans = optionB.getText().toString();
                        break;
                    case R.id.optionC:
                        ans = optionC.getText().toString();
                        break;
                }
            }
        });



        questions.setText(question);
//        questionoptionA.setText(question);

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ans",id+ "  " + ans);
                if (!correctanswer.trim().equals(ans.trim())){
                    marks = "0";
                }else if (correctanswer.trim().equals(ans.trim())){
                    marks = "1";
                }
                Log.e("marks",marks);
                Log.e("correct",correctanswer);
                Log.e("user",ans);

                my_db.updateData(id,ans,marks);
                finish();

            }
        });






    }
}