package com.example.appinnovationtask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.appinnovationtask.ListQuestions.marks;
import static com.example.appinnovationtask.ListQuestions.total_questions;

public class MainActivity extends AppCompatActivity {

    Button add_btn,view_btn;
    DatabaseHelper my_db;
    public  static  int show;
    TextView result;

    String questions[]={"what is my pet name","what is your native","which is the place of college compleition"};
    String optionA[]={"dog","salem","chennai"};
    String optionB[]={"cat","chennai","trichy"};
    String optionC[]={"rabbit","madurai","salem"};
    String correctOptions[]={"dog","salem","salem"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        my_db = new DatabaseHelper(this);


        add_btn = findViewById(R.id.add_btn);
        view_btn = findViewById(R.id.view_btn);
        result = findViewById(R.id.result);


        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i< questions.length;i++){
                    my_db.insertData(questions[i],optionA[i],optionB[i],optionC[i],correctOptions[i],"","");

                }

                Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }
        });


        view_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this,ListQuestions.class);
                startActivity(i);

            }
        });


        marks_obtain();
    }

    private void marks_obtain() {
        if (show == 0){
            result.setVisibility(View.GONE);
        }else {
            result.setVisibility(View.VISIBLE);
            result.setText("Total Marks: "+marks+"/" + total_questions);
        }

    }


    @Override
    protected void onResume() {
        marks_obtain();
        super.onResume();
    }
}