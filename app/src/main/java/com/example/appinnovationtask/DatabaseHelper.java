package com.example.appinnovationtask;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "interviewtask.db";
    public static final String TABLE_NAME = "questionlists";
    public static final String COL1 = "ID";
    public static final String COL2 = "questions";
    public static final String COL3 = "optionA";
    public static final String COL4 = "optionB";
    public static final String COL5 = "optionC";
    public static final String COL6 = "correctOption";
    public static final String COL7 = "userAnwser";
    public static final String COL8 = "marks";

    Context context;
    public DatabaseHelper(@Nullable Context context) {

        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT , questions TEXT , optionA TEXT , optionB TEXT , optionC TEXT , correctOption TEXT,userAnwser TEXT,marks TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String questions, String optionA, String optionB, String optionC, String correctOption, String userAnwser,String marks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, questions);
        contentValues.put(COL3, optionA);
        contentValues.put(COL4, optionB);
        contentValues.put(COL5, optionC);
        contentValues.put(COL6, correctOption);
        contentValues.put(COL7, userAnwser);
        contentValues.put(COL8, marks);


        long succcess = db.insert(TABLE_NAME, null, contentValues);

        if (succcess == -1) {
            return false;
        } else {
            return true;
        }

    }

    public Cursor getAllData() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cr = sqLiteDatabase.rawQuery(" select * from " + TABLE_NAME, null);
        return cr;
    }


    public void updateData(String id , String userAnwser,String marks){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues =new ContentValues();
        contentValues.put(COL7, userAnwser);
        contentValues.put(COL8, marks);
        long succcess = db.update(TABLE_NAME, contentValues,"ID=?",new String[]{id});

       if (succcess == -1){
//           Toast.makeText(context,"fails",Toast.LENGTH_SHORT).show();
       }else {
//           Toast.makeText(context,"success",Toast.LENGTH_SHORT).show();

       }
    }
}
