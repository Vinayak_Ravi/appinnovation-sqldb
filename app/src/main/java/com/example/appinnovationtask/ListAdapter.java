package com.example.appinnovationtask;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static com.example.appinnovationtask.MainActivity.show;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private ArrayList<ListModel> listModel;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView id, question;
        CardView card;


        public MyViewHolder(View view) {
            super(view);
            card = view.findViewById(R.id.card);
            id = view.findViewById(R.id.id_int);
            question = view.findViewById(R.id.question);

        }

    }

    Context context;

    public ListAdapter(ArrayList<ListModel> listModel, Context mContext) {
        this.listModel = listModel;
        this.context = mContext;

    }


    @Override
    public ListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.questions_item, parent, false);


        return new ListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        final ListModel t = listModel.get(position);
        int id = t.id;
        String answer = t.correctAnswer;

        holder.id.setText(String.valueOf(id));
        holder.question.setText(t.questions);

        for(int i = 0; i < listModel.size();i++){
            if(answer.equals("")){
                show = 0;
            }else {
                show =1;
            }
        }



            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, DetailActivity.class);
                    i.putExtra("id", holder.id.getText());
                    i.putExtra("questions", t.questions);
                    i.putExtra("optionA", t.optionA);
                    i.putExtra("optionB", t.optionB);
                    i.putExtra("optionC", t.optionC);
                    i.putExtra("correctanswer", t.correctAnswer);
                    context.startActivity(i);
                }
            });


    }

    @Override
    public int getItemCount() {
        return listModel.size();
    }

}
