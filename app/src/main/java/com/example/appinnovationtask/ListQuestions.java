package com.example.appinnovationtask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.appinnovationtask.MainActivity.show;

public class ListQuestions extends AppCompatActivity {

    RecyclerView list;
    DatabaseHelper my_db;
    ArrayList<HashMap<String, String>> userData;
    private ListAdapter listAdapter;
    private ArrayList<ListModel> list_model = new ArrayList<>();
    TextView result;
    int total;
    public static String marks;
    public static String total_questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_questions);

        my_db = new DatabaseHelper(this);
        userData = new ArrayList<>();

        list = findViewById(R.id.recycler_list);
        result = findViewById(R.id.result);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(mLayoutManager);

        Cursor cr = my_db.getAllData();
        Log.e("cr", String.valueOf(cr));




        while (cr.moveToNext()) {
            ListModel dataModel = new ListModel();
            dataModel.id = cr.getInt(0);
            dataModel.questions = cr.getString(1);
            dataModel.optionA = cr.getString(2);
            dataModel.optionB = cr.getString(3);
            dataModel.optionC = cr.getString(4);
            dataModel.correctAnswer = cr.getString(5);
            dataModel.userAnswer = cr.getString(6);
            dataModel.marks = cr.getString(7);


            int mark = cr.getInt(7);
            total +=  mark;

            list_model.add(dataModel);
        }
        Log.e("sizeofArr", String.valueOf(list_model.size()));
        listAdapter = new ListAdapter(list_model, this);
        list.setAdapter(listAdapter);

        result.setText("Your marks will be Front after completeing the test");

        total_questions = String.valueOf(list_model.size());
        marks = String.valueOf(total);
        result.setVisibility(View.GONE);

        Log.e("total", String.valueOf(total));

    }
}